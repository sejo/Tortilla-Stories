I grew up in Mexico City
Where it is normal to go to a Tortilla store
And get a kilogram of Tortillas or more
Wrapped in paper or in a piece of cloth

Coming from a language with relatively few sounds
It's clear that I wouldn't speak English very well
According to proficiency tests I'm a good listener.
It's never very clear how much of what I say
Is actually understood

After buying some tortillas,
It would be normal to eat one or more,
all by themselves or with some salt.

"Tortilla" in an English accent
sounds too "gringo" for me.
I would say "Tortilla".
However "Tortilla" is a Spaniard name,
"Tlaxcalli" would be the original one
That I didn't know until some weeks ago.

To offer a guest or someone else
a plain tortilla
or a tortilla with salt
would be seen as an act of
bad taste
bad manners
low class
low status

There appears to be not that many
Mexican role models.
In my times as an electrical engineer,
We could have as a reference a man,
We all would learn that he invented 
THE color television.
If you read the history of color television
As told by the English-speaking world,
You will find many people involved,
But not him.

Should I buy local, in the sustainable and cheaper way?
Or should I buy Mexican, supporting the people back there?

There are Mexican stars in Hollywood
But they have to work in the US
Good thing there are tortillas here

Tortillas existed in Mexico before the conquest.
Columbus called "indios" or "indians"
The people that they encountered 
Thinking that they were from the Indies.

Mexican parents in the US
Would not teach Spanish to their children
Indigenous parents in Mexico
Would not teach their native tongue to their children.
In Spanish, Spanish and Spaniard are the same word.

In a way we are proud of our traditions.
But at the same time we apparently despise
Whoever has real bonds with those traditions.

How many brown Mexican students can you find in this building?

La Malinche was an indigenous woman 
That helped Hernan Cortés
To fight against the native people.
From her comes the word "Malinchismo", "Malinchism"
It applies to a lot of the Mexican Culture:
We would rather be from somewhere else
We would rather have things from somewhere else
We would rather study somewhere else

If you call somebody "indio"
It would probably be a big insult
It implies having
bad taste
bad manners
low class
low status
It implies
being poor
being ignorant
being annoying
being uncivilized.

The price of the kilogram of Tortilla in Mexico
Is some kind of local economic indicator.
In this past week the price was about 13 pesos,
That would be like 70 cents.
The Mexican minimum wage is 88 pesos per day of work
That would be like 4.6 dollars.
You could get 6.7 kilograms of Tortillas per day of work.

We don't know how to call somebody
That comes from India
Without feeling that we are insulting them.

I don't have strong nationalist feelings.
I thought I could think of myself
As a citizen of the world in New York.
Here I discovered that I was wrong.
Here the tortillas still connect me back.

The browner your skin is,
The more "indio" you probably are.
Is that good?
Is that bad?

These tortillas are made with the "masa"
Of one of the biggest Mexican multinational corporations.
Maseca.
But the corn comes from Texas,
And the kitchen is from New York.
Are they really Mexican?
And what about me?

Humility and humilliation come from the same Latin root.
Humus, that is earth, ground.
Pushing yourself to the ground could be seen as desirable 
(At least arrogance is normally seen as bad)
Being pushed to the ground is not what someone would want.

Some have said before that our skin 
Has the color of the ground.
Is that good?
Is that bad?

Maybe an advantage of being below already
Is that we can't fall very hard.
Maybe an advantage of being used to simplicity
Is that we can have a good time
Sharing a meal made of corn, limestone, and salt.

It's a good thing that I could find myself in Tortillas.
