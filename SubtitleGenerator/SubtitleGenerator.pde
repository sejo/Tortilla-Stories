// After this program was run, I converted the background to transparent
// with the following command that uses ImageMagick:
// for f in *png; do convert -verbose $f -fuzz 50% -transparent "#00FF00" Transparent/$f; done
String [] lines;

PFont font;

size(1920,1080);
background(0,255,0);
lines = loadStrings("texts.txt");
font = loadFont("Tulia-Bold-100.vlw");

textFont(font, 48);


float maxWidth = width*0.90;
float maxHeight = height*0.2;
float tWidth;
float finalWidth;
float x,y;

noStroke();
String line;
for(int i=0; i<lines.length; i++){
	line = lines[i];
	if(!line.equals("")){
		tWidth = textWidth(line);
		finalWidth = (tWidth>maxWidth)? maxWidth : tWidth;
		x = (width-finalWidth)/2;
		y = height*0.8;
		background(0,255,0);
		fill(0);
		text(line,x+3,y+3,maxWidth,maxHeight);
		fill(255);
		text(line,x,y,maxWidth,maxHeight);
		println(line);
		save(String.format("Line-%03d.png",i));
	}
}

exit();
